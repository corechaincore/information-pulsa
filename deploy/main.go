package main

import (
	"github.com/go-redis/redis"
	"net/http"
	"fmt"
	"strings"
	"io/ioutil"
	"encoding/json"
	"encoding/base64"
	"os"
	"github.com/tealeg/xlsx"
	"github.com/buger/jsonparser"
	"strconv"
	"gopkg.in/robfig/cron.v2"
	"sort"
	"regexp"
	"log"
	"time"
	"encoding/xml"
	"gopkg.in/mgo.v2"
	"crypto/tls"
	"net"
)

const PORT_NUMBER = 6031

//setPrice
const DURATION_SETPRICE = 30 // in Second

//topupScheduler
const LIMIT_SALDO = 500000
const API_KEY = "IfR1TVkeLcRYSbPi0A8MG4IPEtyYZ7he"
const DURATION_TOPUP = 5 // in Minute

//getTicket
const MSISDN = `6281295578742`
const PIN = `F619EA`
const AMOUNT_TOPUP = "300000"

type result struct{
	Attachments []resultDetail
	Subject string
	From string
}

type MethodResponse struct {
	Params struct {
		Param struct {
			Value struct {
				Struct []struct {
					Name string `xml:"name"`
					Value struct {
						String string `xml:"string"`
					} `xml:"value"`
				} `xml:"struct>member"`
			} `xml:"value"`
		} `xml:"param"`
	} `xml:"params"`
}

type resultDetail struct {
	Name        string
	Content     string
	ContentType string
}

type CheckPriceReq struct {
	Total int `json:"total"`
	Data  []struct {
		Kode       string `json:"kode"`
		Namaproduk string `json:"namaproduk"`
		Harga      string `json:"harga"`
		Status     string `json:"status"`
	} `json:"data"`
}

func conn() *redis.Client {
	client := redis.NewClient(&redis.Options{
		//Addr: "localhost:6379",
		Addr:     "35.197.155.228:9967",
		Password: "t3guhCakep", // no password set
		DB:       3,  // use default DB
	})
	return client
}

func connMgo() *mgo.Session {
	var mongoURI = "mongodb://corechain:corechaindb@corechain-shard-00-00-jqy0s.mongodb.net:27017,corechain-shard-00-01-jqy0s.mongodb.net:27017,corechain-shard-00-02-jqy0s.mongodb.net:27017/tagihin?replicaSet=corechain-shard-0&authSource=admin"
	dialInfo, err := mgo.ParseURL(mongoURI)
	if err != nil {
		panic(err)
	}

	//Below part is similar to above.
	tlsConfig := &tls.Config{}
	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}
	session, err := mgo.DialWithInfo(dialInfo)
	session.SetMode(mgo.Monotonic, true)
	return session
}

//const BASE_URL = `http://103.52.147.88:8505/webportal/api/requesttiketdeposit`
const BASE_URL = `https://h2h.anucati.com/api/requesttiketdeposit`

func main() {
	fmt.Println("==== OTP services ====")
	fmt.Println(fmt.Sprintf(" Run on Port :%d", PORT_NUMBER))
	fmt.Println("======================")
	mdb := connMgo()
	defer mdb.Close()
	rdb := conn()
	defer rdb.Close()
	setPrice(rdb)
	topupScheduler(rdb,mdb)
	mux := http.NewServeMux()
	mux.HandleFunc("/health", health)
	mux.HandleFunc("/apiv1/biller/price/", price(rdb))
	mux.HandleFunc("/apiv1/biller/pricelist/", pricelist(rdb))
	mux.HandleFunc("/apiv1/biller/inbound", inbound(rdb))
	mux.HandleFunc("/apiv1/biller/transaction", transaction(rdb))
	mux.HandleFunc("/apiv1/biller/checkBalances", checkBalances(rdb))
	mux.HandleFunc("/apiv1/biller/getTicket", getTicket(rdb))
	http.ListenAndServe(fmt.Sprintf(":%d", PORT_NUMBER), mux)
}

func health(w http.ResponseWriter, r *http.Request){
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message":"HEALTH"}`))
}

func price(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		message := ""
		status := http.StatusOK
		uri := r.RequestURI
		arrUri := strings.Split(uri,"/")
		operator := arrUri[4]
		prices := arrUri[5]
		keys := fmt.Sprintf("%s:%s",strings.ToUpper(operator),prices)
		val,err := rdb.Get(keys).Result()
		if err == redis.Nil {
			status = http.StatusNotFound
			message = `{"Message":"Data Not Found"}`
		}else{
			arrVal := strings.Split(val,":" +
				"")
			if(len(arrVal) == 2){
				code := arrVal[0]
				price := arrVal[1]
				message = `{"code":"`+code+`","price":"`+price+`"}`
			}else{
				status = http.StatusNotFound
				message = `{"Message":"Data Not Found"}`
			}
		}
		w.Write([]byte(message))
		w.WriteHeader(status)
	}
}

func pricelistOrder(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		message := ""
		status := http.StatusOK
		uri := r.RequestURI
		arrUri := strings.Split(uri,"/")
		operator := arrUri[4]
		keys := fmt.Sprintf("%s:*",strings.ToUpper(operator))
		val,err := rdb.Keys(keys).Result()
		if err == redis.Nil {
			status = http.StatusNotFound
			message = `{"Message":"Data Not Found"}`
		}else{
			fmt.Println(val[0])
			lens := len(val)
			var gbArr []float64
			var mbArr []float64
			var nomArr []int
			for i := 0; i < lens; i++ {
				valArray := strings.Split(val[i],":")
				vals := strings.ToUpper(valArray[1])
				if(strings.Contains(vals,"GB")){
					splitGbs := strings.Replace(vals,"GB","",-1)
					splitGb := strings.Replace(splitGbs,",",".",-1)
					valueGb,_ := strconv.ParseFloat(splitGb,64)
					gbArr = append(gbArr,valueGb)
				}else if(strings.Contains(vals,"MB")){
					splitMbs := strings.Replace(vals,"MB","",-1)
					splitMb := strings.Replace(splitMbs,",",".",-1)
					valueMb,_ := strconv.ParseFloat(splitMb,64)
					mbArr = append(mbArr,valueMb)
				}else{
					value,_ := strconv.Atoi(vals)
					nomArr = append(nomArr,value)
				}
			}
			sort.Float64s(gbArr)
			sort.Float64s(mbArr)
			sort.Ints(nomArr)
			message = `{"data":[`

			for j := 0; j < len(mbArr); j++ {
				a := strconv.FormatFloat(mbArr[j],'f', 0, 64)
				denomA := fmt.Sprintf("%sMB",a)
				keyA := fmt.Sprintf("%s:%s",strings.ToUpper(operator),denomA)
				values,err := rdb.Get(keyA).Result()
				if err != redis.Nil {
					arrVal := strings.Split(values,":" +
						"")
					if(len(arrVal) == 2) {
						price := arrVal[1]
						message = fmt.Sprintf(`%s ,{"denom":"%s","price":%s}`,message,denomA,price)
					}
				}
			}

			for k := 0; k < len(gbArr); k++ {
				b := gbArr[k]
				/*intB,_ := fmt.Printf("%.0f", b)
				if(b > intB){

				}*/

				denomB := fmt.Sprintf("%fGB",b)
				keyB := fmt.Sprintf("%s:%s",strings.ToUpper(operator),denomB)
				values,err := rdb.Get(keyB).Result()
				if err != redis.Nil {
					arrVal := strings.Split(values,":" +
						"")
					if(len(arrVal) == 2) {
						price := arrVal[1]
						message = fmt.Sprintf(`%s ,{"denom":"%s","price":%s}`,message,denomB,price)
					}
				}
			}

			for l := 0; l < len(nomArr); l++ {
				denom := nomArr[l]
				key := fmt.Sprintf("%s:%d",strings.ToUpper(operator),denom)
				values,err := rdb.Get(key).Result()
				if err != redis.Nil {
					arrVal := strings.Split(values,":" +
						"")
					if(len(arrVal) == 2) {
						price := arrVal[1]
						message = fmt.Sprintf(`%s ,{"denom":"%d","price":%s}`,message,denom,price)
					}
				}
			}
			message = strings.Replace(message,",","",1)
			message = fmt.Sprintf(`%s ]}`,message)
			fmt.Println("Strings:", message)

		}
		w.Write([]byte(message))
		w.WriteHeader(status)
	}
}

func pricelist(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		message := ""
		status := http.StatusOK
		uri := r.RequestURI
		arrUri := strings.Split(uri,"/")
		operator := strings.ToUpper(arrUri[4])
		keys := fmt.Sprintf("%s:*",operator)
		val,err := rdb.Keys(keys).Result()
		if err == redis.Nil {
			status = http.StatusNotFound
			message = `{"Message":"Data Not Found"}`
		}else{
			lens := len(val)
			if(lens > 0) {
				sort.Strings(val)
				for i := 0; i < lens; i++ {
					valArray := strings.Split(val[i], ":")
					vals := strings.ToUpper(valArray[1])
					keyA := fmt.Sprintf("%s:%s", strings.ToUpper(operator), vals)
					values, err := rdb.Get(keyA).Result()
					if err != redis.Nil {
						arrVal := strings.Split(values, ":"+
							"")
						if (len(arrVal) == 3) {
							codeBiller := arrVal[0]
							price := arrVal[1]
							types := arrVal[2]
							message = fmt.Sprintf(`%s ,{"provider":"%s","kode":"%s","voucher":"%s","denom":"%s","price":%s,"type":"%s"}`,message, operator, codeBiller, vals, vals, price,types)
						}
					}

				}
				message = fmt.Sprintf(`{"data":[ %s`, message)
				message = strings.Replace(message, ",", "", 1)
				message = fmt.Sprintf(`%s ]}`, message)
				//fmt.Println("Strings:", message)
			}else{
				status = http.StatusNotFound
				message = `{"Message":"Data Not Found"}`
			}
		}
		w.Write([]byte(message))
		w.WriteHeader(status)
	}
}

func inbound(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request){
		message := ""
		body,err := ioutil.ReadAll(r.Body)
		//println(string(body))
		response := result{}
		if err == nil {
			err = json.Unmarshal(body, &response)
			if err == nil {
				from := strings.Trim(strings.ToLower(response.From),"")
				if(validation(rdb,from)) {
					if(strings.Compare(strings.Trim(strings.ToUpper(response.Subject),""),"UPDATE HARGA BILLER")==0) {
						filename := response.Attachments[0].Name
						data, err := base64.StdEncoding.DecodeString(response.Attachments[0].Content)
						if err != nil {
							message = `Update Biller Gagal - Attachment Tidak Ditemukan atau Format Tidak Valid`
						}else {

							//fmt.Printf("%q\n", data)
							f, err := os.Create(filename)
							if err != nil {
								panic(err)
							}
							defer f.Close()

							if _, err := f.Write(data); err != nil {
								panic(err)
							}
							if err := f.Sync(); err != nil {
								panic(err)
							}

							xlFile, err := xlsx.OpenFile(filename)
							if err != nil {
								fmt.Print(err)
							}
							itr := 0
							//fsheet := xlFile.Sheet["price"]
							for _, fsheet := range xlFile.Sheets {
								for _, row := range fsheet.Rows {
									if (itr > 0) {
										slice := []string{}
										for _, cell := range row.Cells {
											text := cell.String()
											slice = append(slice, text)
										}
										msg, _ := insertData(rdb, slice)
										message = msg
									}
									itr++
								}
							}
							defer os.Remove(filename)
						}
					}else{ //else subject
						message = `Update Biller Gagal - Format Subject Email Salah`
					}
				}else{ //else from
					message = `Update Biller Gagal - Alamat Email Tidak Terdaftar`
				}
			}else{ //else unmarshall
				message = `Update Biller Gagal - Convert Content Gagal`
			}
		}else{ //read body
			message = `Update Biller Gagal - Baca Body API Gagal`
		}
		fmt.Println(message)
	}
}

func insertData(rdb *redis.Client,slice []string)(message string,err error){
	operator := slice[0]
	nominal := slice[1]
	code := slice[2]
	price := slice[3]
	types := slice[4]

	keys := fmt.Sprintf("%s:%s",operator,nominal)
	values := fmt.Sprintf("%s:%s:%s",code,price,types)

	err = rdb.Set(keys, values, 0).Err()
	if err != nil {
		message = `Update Biller Gagal - Simpan Data`
	}else{
		message = `Update Biller Sukses`
	}
	return message,err
}

func setPrice(rdb *redis.Client){
	const LOGS = "logSetPrice"
	c := cron.New()
	cronSpec := fmt.Sprintf("@every %ds",DURATION_SETPRICE)
	c.AddFunc(cronSpec,
		func() {
			dates := time.Now().Unix()
			url := "https://h2h.anucati.com/api/cekhargamitra?pin=F619EA&hp=6281295578742"

			req, _ := http.NewRequest("GET", url, nil)

			res, _ := http.DefaultClient.Do(req)

			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			var dataJson = []byte(body)
			var datas CheckPriceReq
			json.Unmarshal(dataJson, &datas)

			reg, err := regexp.Compile("[^0-9]+")
			if err != nil {
				log.Fatal(err)
				logs := fmt.Sprintf("%d:%s",dates,err.Error())
				rdb.LPush(LOGS,logs,0)
			}

			for _,data := range datas.Data{
				var arrData []string
				productName := strings.ToUpper(data.Namaproduk)
				codeBiller := data.Kode
				price := strings.Replace(data.Harga,".","",-1)
				denom := reg.ReplaceAllString(codeBiller, "")
				quota := ""
				types := ""
				operator := strings.ToUpper(strings.Split(data.Namaproduk," ")[0])
				if(strings.Contains(operator,"ISAT")){
					operator = "INDOSAT"
				}
				if(strings.Contains(productName,"DATA")){
					types = "DATA"
					if(strings.Contains(productName,"GB")){
						quota = "GB"
					}else if(strings.Contains(productName,"MB")){
						quota = "MB"
					}else{
						quota = "000"
					}
					denom = fmt.Sprintf("%s%s",denom, quota)
				}else{
					types = "REG"
					quota = "000"
					denom = fmt.Sprintf("%s%s",denom, quota)
				}
				arrData = append(arrData, operator,denom,codeBiller,price,types)
				_,err = insertData(rdb,arrData)
			}
			if err != nil {
				logs := fmt.Sprintf("%d:%s",dates,err.Error())
				rdb.LPush(LOGS,logs,0)
			}
			})
	c.Start()
}

func validation(redist *redis.Client,email string)(result bool){
	keys := "whitelistEmail"
	_, err := redist.LRange(keys, 0, -1).Result()
	if err != redis.Nil {
		lists := redist.LRange(keys, 0, -1).Val()
		result = false
		for _, v := range lists {
			if(strings.Compare(strings.Trim(v,""),email)==0){
				result = true
				break
			}
		}
	}
	return result
}

func transaction(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		status := http.StatusOK
		message := `{"Message":"Transaction Success"}`
		bodyReq, _ := ioutil.ReadAll(r.Body)
		operator,_ := jsonparser.GetString(bodyReq, "operator")
		price,_ := jsonparser.GetInt(bodyReq, "price")
		strPrice := strconv.FormatInt(price,10)
		keys := fmt.Sprintf("%s:%s",operator,strPrice)
		valArr,err := rdb.Get(keys).Result()
		if err == redis.Nil {
			status = http.StatusNotFound
			message = `{"Message":"Biller Code Not Found"}`
		}else{
			nom := strings.Split(valArr,":")[0]
			reqIdDefault := 110022010
			reqKeys := "lastRequestId"
			requestid := ""
			lastRequestid,err := rdb.Get(reqKeys).Result()
			if err == redis.Nil {
				rdb.Set(reqKeys,reqIdDefault,-1)
				requestid = strconv.Itoa(reqIdDefault)
			}else{
				val,_ := strconv.Atoi(lastRequestid)
				requestid = fmt.Sprintf("%d",val + 1)
			}

			msisdn := "6281295578742"
			pin := "F619EA"
			phone,_ := jsonparser.GetString(bodyReq, "phone")
			rdr := fmt.Sprintf(`<?xml version="1.0"?><methodCall><methodName>topUpInquiry</methodName><params><param><value><struct><member><name>MSISDN</name><value><string>%s</string></value></member><member><name>REQUESTID</name><value><string>%s</string></value></member><member><name>PIN</name><value><string>%s</string></value></member><member><name>NOHP</name><value><string>%s</string></value></member><member><name>NOM</name><value><string>%s</string></value></member></struct></value></param></params></methodCall>`,msisdn,requestid,pin,phone,nom)
			url := "http://h2h.anucati.com/"

			payload := strings.NewReader(rdr)

			req, _ := http.NewRequest("POST", url, payload)

			req.Header.Add("content-type", "application/xml")

			res, _ := http.DefaultClient.Do(req)

			defer res.Body.Close()
			body, _ := ioutil.ReadAll(res.Body)

			rdb.Set(reqKeys,requestid,-1)

			fmt.Println(string(body))
		}

		w.WriteHeader(status)
		w.Write([]byte(message))

	}
}

func topupScheduler(rdb *redis.Client,mdb *mgo.Session) {
	fmt.Println("-------START SCHEDULER TICKET-------")
	c := cron.New()
	cronSpec := fmt.Sprintf("@every %dm",DURATION_TOPUP)
	c.AddFunc(cronSpec, func() {
		url := "http://localhost:6031/apiv1/biller/checkBalances"
		req, _ := http.NewRequest("GET", url, nil)
		res, _ := http.DefaultClient.Do(req)
		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)
		fmt.Println("check balances : "+string(body))
		if res.StatusCode == http.StatusOK{
			strSaldo,_ := jsonparser.GetString(body, "saldo")
			intSaldo,_ := strconv.Atoi(strSaldo)
			if intSaldo < LIMIT_SALDO{
				url := "http://localhost:6031/apiv1/biller/getTicket"
				req, _ := http.NewRequest("GET", url, nil)
				res, _ := http.DefaultClient.Do(req)
				defer res.Body.Close()
				body, _ := ioutil.ReadAll(res.Body)
				fmt.Println("get ticket : "+string(body))

				if res.StatusCode == http.StatusOK{
					accountNumber,_ := jsonparser.GetString(body,"accountNumber")
					accountName,_ := jsonparser.GetString(body,"accountName")
					amount,_ := jsonparser.GetInt(body,"amount")
					bankName,_ := jsonparser.GetString(body,"bankName")
					ticket,_ := jsonparser.GetInt(body,"ticket")

					/*accountNumber = "1440016961101"
					accountName = "Danang Kukuh Pribadi"
					amount = 10
					bankName = "MANDIRI"*/

					/*
						{
						  "AccountFrom": "860005525500",
						  "AccountTo": "1440016961101",
						  "AccountToName": "Danang Kukuh Pribadi",
						  "Amount": 10,
						  "AccountToBankCode": "008",
						  "AccountToBankName": "BANK MANDIRI",
						  "PhoneNumber": "081285125258"
						}
					*/

					var bankNameFull, bankCode string
					bankName = strings.ToUpper(bankName)
					switch bankName {
					case "BCA":
						bankNameFull = "BANK BCA"
						bankCode = "014"
					case "MANDIRI":
						bankNameFull = "BANK MANDIRI"
						bankCode = "008"
					case "BNI":
						bankNameFull = "BANK BNI"
						bankCode = "009"
					default:
						bankNameFull = "BANK BCA"
						bankCode = "014"
					}

					url := "http://35.198.202.232:2002/apiv1/cashout/cimb/other"
					params := fmt.Sprintf("{\n  \"AccountFrom\": \"860005525500\",\n  \"AccountTo\": \"%s\",\n  \"AccountToName\": \"%s\",\n  \"Amount\": %d,\n  \"AccountToBankCode\": \"%s\",\n  \"AccountToBankName\": \"%s\",\n  \"PhoneNumber\": \"081285125258\"\n}",
						accountNumber,accountName,amount,bankCode,bankNameFull)
					fmt.Println("cashout params : "+params)
					payload := strings.NewReader(params)
					req, _ := http.NewRequest("POST", url, payload)
					req.Header.Add("content-type", "application/json")
					req.Header.Add("apikey", API_KEY)

					res, _ := http.DefaultClient.Do(req)

					defer res.Body.Close()
					body, _ := ioutil.ReadAll(res.Body)
					fmt.Println("cashout : "+string(body))

					unixTime := time.Now().Unix()
					tm := time.Unix(unixTime, 0)
					datetime := tm.Format("20060102")
					indatetime,_ := strconv.Atoi(datetime)
					objMap := make(map[string]interface{})

					if res.StatusCode == http.StatusOK{
						objMap["success"] = true
					}else{
						objMap["success"] = false
						objMap["errorMessage"] = string(body)
					}

					objMap["ticket"] = ticket
					objMap["amount"] = amount
					objMap["bankName"] = bankName
					objMap["accountNumber"] = accountNumber
					objMap["accountName"] = accountName
					objMap["timestamp"] = unixTime
					objMap["dates"] = indatetime

					c := mdb.DB("anucati-biller").C("logTopup")

					c.Insert(objMap)
				}
			}
		}
	})
	c.Start()
}

func checkBalances(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
		return func(w http.ResponseWriter, r *http.Request) {
			status := http.StatusOK
			message := `{"Message":"Transaction Success"}`
			reqKeys := "lastRequestId"
			lastRequestid,err := rdb.Get(reqKeys).Result()
			if err == redis.Nil {
				status = http.StatusInternalServerError
				message = `{"Message":"Service Not Available"}`
			}else {
				msisdn := "6281295578742"
				phone := "081295578742"
				pin := "F619EA"
				nom := "THN10"
				rdr := fmt.Sprintf(`<?xml version="1.0"?><methodCall><methodName>topUpInquiry</methodName><params><param><value><struct><member><name>MSISDN</name><value><string>%s</string></value></member><member><name>REQUESTID</name><value><string>%s</string></value></member><member><name>PIN</name><value><string>%s</string></value></member><member><name>NOHP</name><value><string>%s</string></value></member><member><name>NOM</name><value><string>%s</string></value></member></struct></value></param></params></methodCall>`, msisdn, lastRequestid, pin, phone, nom)
				url := "http://h2h.anucati.com/"

				payload := strings.NewReader(rdr)

				req, _ := http.NewRequest("POST", url, payload)

				req.Header.Add("content-type", "application/xml")

				res, _ := http.DefaultClient.Do(req)

				defer res.Body.Close()
				body, _ := ioutil.ReadAll(res.Body)

				bodys := strings.Replace(string(body),`<?xml version="1.0"?>`,"",-1)

				strAmount := ""
				var dataXML = []byte(bodys)
				var datas MethodResponse
				err := xml.Unmarshal(dataXML, &datas)
				if err != nil {
					status = http.StatusInternalServerError
					message = `{"Message":"Service Not Available"}`
				}else{
					respCode := datas.Params.Param.Value.Struct[0].Value.String
					respMsg := datas.Params.Param.Value.Struct[2].Value.String
					fmt.Println(respMsg)
					if(strings.Compare(respCode,"02") == 0) {
						//respMsg = `Trx THN10 ke 081295578742 GAGAL. mohon diperiksa kembali No tujuan sebelum di ulang. Saldo: Rp 276.376. 06/03/2018`
						strAmount = strings.Replace(strings.Split(strings.Trim(strings.Split(strings.ToLower(respMsg),"rp")[1]," ")," ")[0],".","",-1)
						message = fmt.Sprintf(`{"saldo":%s}`,strAmount)
					}else if(strings.Compare(respCode,"00") == 0) {
						//respMsg = `SN:0041002218204770;Trx THN20.082199645601 BERHASIL,Harga: 20.000 SN: 0041002218204770 Sisa Saldo: 337.376 - 20.000 = 317.376 @2018/03/05 11:04:46`
						strAmount = strings.Replace(strings.Split(strings.Trim(strings.Split(strings.ToLower(respMsg),"=")[1]," ")," ")[0],".","",-1)
						message = fmt.Sprintf(`{"saldo":%s}`,strAmount)
					}else{
						status = http.StatusInternalServerError
						message = `{"Message":"Service Not Available"}`
					}
					/*fmt.Println(strAmount)
					fmt.Println(datas)
					fmt.Println(datas.Params.Param.Value.Struct[2].Value)*/
				}
			}
			w.WriteHeader(status)
			w.Write([]byte(message))
		}
}

func getTicket(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusInternalServerError
		message := `{"message":"Service Unavailable"}`

		url := fmt.Sprintf(``+BASE_URL+`?hp=`+MSISDN+`&pin=`+PIN+`&jumlah=`+AMOUNT_TOPUP+``)
		payload := strings.NewReader("")
		req, _ := http.NewRequest("GET", url, payload)
		res, _ := http.DefaultClient.Do(req)
		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)
		status,_ := jsonparser.GetBoolean(body, "success")
		if(status){
			msg,_ := jsonparser.GetString(body, "msg")
			ticket,_ := jsonparser.GetInt(body, "tiket")
			//msg := `Silahkan transfer sebesar 509054 (harus sama persis). ke \r\nBCA:1234567890 \r\nMANDIRI: xxx\r\nBRI xxx \r\nA\/N:xxxx","tiket":509054`
			//msg := `Silahkan transfer sebesar 509212 (harus sama persis). ke \r\nBCA: 5855313577\r\nA\/N: DONALD AKBAR\r\n\r\nBNI: 8189005584\r\nA\/N: DONALD AKBAR\r\n\r\n*  LIMIT TIKET 15 MENIT\r\n**TIKET DEPOSIT CLOSED PUKUL 20:30`
			arrMsg := strings.Split(msg," ")
			amount,err := strconv.ParseFloat(arrMsg[3],64)
			fmt.Println(amount)
			if(err == nil){
				reg, err := regexp.Compile("[^0-9]+")
				if err != nil {
					log.Fatal(err)
				}
				accountNumber := strings.Trim(reg.ReplaceAllString(arrMsg[9], ""),"")

				regBankName, err := regexp.Compile("[^A-Z]+")
				fmt.Println(accountNumber)
				if err != nil {
					log.Fatal(err)
				}

				accountNameFirst := strings.Trim(regBankName.ReplaceAllString(arrMsg[10], ""),"")
				fmt.Println(accountNameFirst)

				accountNameLast := strings.Trim(strings.Split(arrMsg[11], "\r")[0],"")
				fmt.Println(accountNameLast)

				accountName := fmt.Sprintf("%s %s",accountNameFirst,accountNameLast)

				bankName := strings.Trim(regBankName.ReplaceAllString(arrMsg[8], ""),"")
				fmt.Println(bankName)

				message = fmt.Sprintf(`{"amount":%.0f,"bankName":"%s","accountNumber":"%s","accountName":"%s","ticket":%d}`, amount, bankName, accountNumber, accountName, ticket)
				fmt.Println(message)
				code = http.StatusOK
			}
		}
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}